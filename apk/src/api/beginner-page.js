const http = uni.$u.http

export function GetChapterList(){
    return http.get('/resource/base_init.json'+'?t=' +Math.random())
}
export function PostChapterById(chatpter_id){
    return http.get(`/resource/${chatpter_id}/base_init_chapters.json`+'?t=' +Math.random())
}
