const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const admin_user_id = new mongoose.Types.ObjectId()
const admin_role_id = new mongoose.Types.ObjectId()
module.exports = {
    users: [{
        _id: admin_user_id,
        "username": "admin",
        "password": "123456",
        "phone": "18888888888",
        "nickname": "",
        "email": "admin@example.com",
        "location": "New York City",
        "title": "CTO",
        "description": null,
        "tags": null,
        "avatar": null,
        "theme": "auto",
        "tfa_secret": null,
        "status": "active",
        "roles": [admin_role_id],
        "token": null,
        "last_access": "2021-02-05T10:18:13-05:00",
        "last_page": "/settings/roles/653925a9-970e-487a-bfc0-ab6c96affcdc"
    }],
    roles: [
        {
            _id: admin_role_id,
            "name": "Admin",
            "icon": "supervised_user_circle",
            "description": null,
            "ip_access": null,
            "enforce_tfa": false,
            "admin_access": true,
            "app_access": true,
            "users": admin_user_id
        }
    ],
}